drop database uas_pbo;
create database uas_pbo;
use uas_pbo;

create table BankAccount (
	recNum varchar(15) PRIMARY KEY,
    name text,
    total bigint
);

create table BankTransaction (
	slipNum bigint AUTO_INCREMENT PRIMARY KEY,
    recNum varchar(15),
    transcDate text,
    transcType text,
    total bigint
);