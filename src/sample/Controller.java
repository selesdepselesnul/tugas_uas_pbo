package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.text.Text;

import java.net.URL;
import java.sql.*;
import java.time.LocalDate;
import java.util.ResourceBundle;
import java.util.function.Consumer;

public class Controller implements Initializable{

    @FXML
    public Text stringMoneyText;
    @FXML
    public TextField transactionTotalTextField;
    @FXML
    public ComboBox<String> transcTypeComboBox;
    @FXML
    public TextField slipNumTextField;
    @FXML
    public TextField recNumTextField;
    @FXML
    public DatePicker transcDatePicker;
    @FXML
    public TextField nameTextField;

    private Connection con;

    public void onDisplay(ActionEvent actionEvent) {
        Terbilang terbilang = new Terbilang(transactionTotalTextField.getText());
        stringMoneyText.setText(terbilang.toString());
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        transcTypeComboBox.getItems().addAll("Debit", "Credit");
        transcTypeComboBox.setValue("Debit");
        try {
            con = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/uas_pbo",
                    "root",
                    "");
            Statement stat = con.createStatement();
            ResultSet rs = stat.executeQuery("select * from banktransaction order by slipNum desc");
            if(!rs.next())
                slipNumTextField.setText("1");
            else
                slipNumTextField.setText((rs.getLong("slipNum") + 1) + "");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void doTransaction() throws SQLException {
        String recNum = recNumTextField.getText();
        PreparedStatement selectBA = con.prepareStatement("select * from bankaccount where recNum = ? and name = ?");
        selectBA.setString(1, recNum);
        selectBA.setString(2, nameTextField.getText());
        ResultSet selectBAResultSet = selectBA.executeQuery();
        Runnable insertTransaction = () -> {
            PreparedStatement insertBTPrep = null;
            try {
                insertBTPrep = con.prepareStatement(
                        "insert into banktransaction(recNum, transcDate, transcType, total) values (?, ?, ?, ?);");
                insertBTPrep.setString(1, recNum);
                insertBTPrep.setString(2, transcDatePicker.getValue() == null ? "" : transcDatePicker.getValue().toString());
                insertBTPrep.setString(3, transcTypeComboBox.getValue());
                insertBTPrep.setLong(4, Integer.parseInt(transactionTotalTextField.getText()));
                insertBTPrep.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        };

        Consumer<Long> insertBT = x -> {
            PreparedStatement insertBAPrep = null;
            try {
                insertBAPrep = con.prepareStatement(
                        "update bankaccount set total = ? where recNum = ?");
                insertBAPrep.setLong(1, x);
                insertBAPrep.setString(2, recNum);
                insertBAPrep.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        };

        Consumer<Long> successMessage = x -> {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Berhasil");
            alert.setContentText("Berhasil diproses, saldo = " + x);
            slipNumTextField.setText(
                    (Long.parseLong(slipNumTextField.getText()) + 1) + "" );
            nameTextField.clear();
            transactionTotalTextField.clear();
            recNumTextField.clear();
            stringMoneyText.setText("");
            transcDatePicker.setValue(null);
            transcTypeComboBox.setValue("Debit");
            alert.showAndWait();
        };

        if(selectBAResultSet.next()) {
            long total = selectBAResultSet.getLong("total");
            long totalFromUser = Long.parseLong(transactionTotalTextField.getText());
            if(transcTypeComboBox.getSelectionModel().getSelectedItem().equals("Debit")) {
                long resultTotal = total - totalFromUser;
                if(resultTotal <= 50000) {
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Tidak valid");
                    alert.setHeaderText("Invalid jumlah");
                    alert.setContentText("Melewati limit, saldo anda sekarang = " + total);

                    alert.showAndWait();
                } else {
                    insertBT.accept(resultTotal);
                    insertTransaction.run();
                    successMessage.accept(resultTotal);
                }
            } else {
                long currentVal = total + totalFromUser;
                insertBT.accept(currentVal);
                insertTransaction.run();
                successMessage.accept(currentVal);
            }
        } else {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Tidak valid");
            alert.setHeaderText("Invalid akun");
            alert.setContentText("Akun tidak ditemukan");

            alert.showAndWait();
        }
    }

    public void onSave(ActionEvent actionEvent) {
        try {
            String recNum = recNumTextField.getText();
            String rawTransactionTotal = transactionTotalTextField.getText();
            String name = nameTextField.getText();
            LocalDate rawDate = transcDatePicker.getValue();

            if(recNum.equals("")
                    || rawTransactionTotal.equals("")
                    || name.equals("")
                    || rawDate == null) {
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Tidak valid");
                alert.setContentText("Tidak boleh ada field yang kosong!");
                alert.showAndWait();
            } else {
                doTransaction();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
